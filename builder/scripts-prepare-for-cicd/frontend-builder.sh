#!/bin/bash
# ./backend-builder.sh "iot-product-center iot-device-center iot-ota-center iot-data-center"
# ./frontend-builder.sh "owner-web positec-web"

BasicWorkDir=$(pwd)
All_Project_Will_Deploy=$1
DockerImageRepoGroup=iot-dev
DockerImageName=''
ECR_STR="680218613974.dkr.ecr.us-east-1.amazonaws.com"
AWS_REGION="us-east-1"
EKS_CLUSTER_PREFIX="PositecEksDev"
#PROJECT_NAME="hello-go-demo"
CONTAINER_NAME="${DockerImageRepoGroup}/${PROJECT_NAME}"
#CONTAINER_IMAGE="$CONTAINER_NAME:$VERSION"
CreateTime=$(date +%Y%m%d-%H%M)
VERSION=dev-${CreateTime}
#在编译时使用国内源
IsInChina=true
#编译源码方式有docker,local 两种
#export BuildMethod=docker
export BuildMethod=local


#项目清单,预留
BackendServiceList=( iot-product-center iot-device-center iot-ota-center iot-data-center )
FrontendServiceList=( owner-web positec-web )

#NodeJs所需变量



if [[ ! $1 ]];then
	echo No PROJECT WILL Deploy,exiting....
	exit 1
fi



GenerateNodejsDockerfile(){
if [[ $BuildMethod == 'local' ]];then
cat > .dockerignore <<EOF
node_modules
EOF


#DockerfileV1使用编译好的文件进行镜像制作
cat > Dockerfile << EOF
FROM nginx:alpine
ENV TZ Asia/Shanghai
#ENV TdsEnv $TdsEnv
RUN echo \$TZ  >/etc/timezone

RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories \
    && apk add --no-cache nano

#COPY nginx-${TdsEnv}.conf /etc/nginx/nginx.conf
COPY nginx.conf /etc/nginx/nginx.conf
RUN  rm  /etc/nginx/conf.d/default.conf
COPY entrypoint.sh /usr/local/project/
RUN  chmod +x /usr/local/project/entrypoint.sh
#COPY ./ssl/. /usr/local/project/ssl/
COPY ./dist  /usr/share/nginx/html/
#COPY ./DnsList.txt /project/
WORKDIR /etc/nginx/
#ENTRYPOINT ["sh", "/project/entrypoint.sh"]
CMD ["sh", "/usr/local/project/entrypoint.sh"]
EOF
fi

if [[ $BuildMethod == 'docker' ]];then
cat > .dockerignore <<EOF
#no_use
EOF

#DockerfileV2使用docker编译
cat > Dockerfile << EOF
FROM node:16-alpine AS builder1
WORKDIR /app
COPY  package.json  ./
#使用下面这段,需要开启BuildKit:DOCKER_BUILDKIT=1
RUN --mount=type=cache,id=build_for_npm_module,target=/app/node_modules,sharing=locked \
    --mount=type=cache,id=build_for_npm_cache,target=/root/.npm \
    npm install --registry https://registry.npmmirror.com
RUN ls -lh && pwd && du -sh node_modules

FROM node:16-alpine AS builder2
WORKDIR /app
COPY  ./  ./
COPY --from=builder1 /app/node_modules ./node_modules
RUN npm run build
RUN ls -lh && pwd && du -sh node_modules


FROM nginx:alpine
#ENV TZ Asia/Shanghai
ENV TZ Etc/UTC
RUN ln -fs /usr/share/zoneinfo/\${TZ} /etc/localtime \
    && echo \${TZ} > /etc/timezone
COPY --from=builder2 /app/dist/.  /usr/share/nginx/html/
RUN --mount=type=cache,id=for_apk_install,target=/var/cache/apk,sharing=locked \
    sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories  \
    && apk add  nano

COPY nginx.conf /etc/nginx/nginx.conf
RUN  rm  /etc/nginx/conf.d/default.conf
COPY entrypoint.sh /usr/local/project/
RUN  chmod +x /usr/local/project/entrypoint.sh
#COPY ./ssl/. /usr/local/project/ssl/
#COPY ./dist  /usr/share/nginx/html/
#COPY ./DnsList.txt /project/
WORKDIR /etc/nginx/
#ENTRYPOINT ["sh", "/project/entrypoint.sh"]
CMD ["sh", "/usr/local/project/entrypoint.sh"]
EOF
fi
}

GenerateNodejsEntrypoint(){
# 变量前的'\'在EOF中,不要删除
cat > entrypoint.sh << EOF
#!/bin/sh

#cat /project/DnsList.txt >> /etc/hosts

nginx -g 'daemon off;'

EOF
}


NpmBuildNodeJs(){
  if [[ $(which npm) ]];then
    echo "----------- Compiling the NodeJS source code  ----------------------------"
    #npm config set registry=http://registry.npm.taobao.org
    npm install
    npm run build
  else
    echo "Please install npm"
  fi
}



GenerateAppDeployment(){


#定义服务端口
ContainerPort=80

# 变量前的'\'在EOF中,不要删除
cat > AppDeployment.yml << EOF
apiVersion: v1
kind: Namespace
metadata:
  name: default
---
apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: default
  name: ${PROJECT_NAME}
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: ${PROJECT_NAME}
  replicas: 1
  template:
    metadata:
      labels:
        app.kubernetes.io/name: ${PROJECT_NAME}
    spec:
      containers:
      - image: ${ECR_STR}/${DockerImageRepoGroup}/${PROJECT_NAME}:${VERSION}
        imagePullPolicy: Always
        name: ${PROJECT_NAME}
        ports:
        - containerPort: $ContainerPort
---
apiVersion: v1
kind: Service
metadata:
  namespace: default
  name: ${PROJECT_NAME}
spec:
  ports:
    - port: ${ContainerPort}
      targetPort: ${ContainerPort}
      protocol: TCP
  type: ClusterIP
  selector:
    app.kubernetes.io/name: ${PROJECT_NAME}
# ---
# apiVersion: extensions/v1beta1
# kind: Ingress
# metadata:
#   namespace: default
#   name: ${PROJECT_NAME}
#   annotations:
#     kubernetes.io/ingress.class: alb
#     alb.ingress.kubernetes.io/scheme: internet-facing
#     alb.ingress.kubernetes.io/target-type: ip
# spec:
#   rules:
#     - http:
#         paths:
#           - path: /*
#             backend:
#               serviceName: ${PROJECT_NAME}
#               servicePort: ${ContainerPort}
EOF
cat AppDeployment.yml
}

DockerImageRepoAuthSetForAwsEcr(){
  #if [[ $(cat ~/.docker/config.json |grep ${ECR_STR})  ]];then
  #  echo "you can already push images to $ECR_STR"
  #else
    #令牌有效时间为12小时,因此建议每次运行脚本时候生成一次
    aws ecr get-login-password --region ${AWS_REGION} | docker login --username AWS --password-stdin $ECR_STR
  #fi
  aws --region ${AWS_REGION} ecr describe-repositories --repository-names ${DockerImageRepoGroup}/${PROJECT_NAME} || aws --region ${AWS_REGION} ecr create-repository --repository-name ${DockerImageRepoGroup}/${PROJECT_NAME}
}


DockerBuild(){
  if [[ $BuildMethod == 'docker' ]];then
    DockerBuildArgs='--progress=plain'
    #docker版本大于19.03才能启用buildkit
    DockerVersion=$(docker -v|awk '{print $3}'|tr -d ',')
    export DOCKER_BUILDKIT=1
    #避免出现output clipped, log limit 1MiB reached问题
    export BUILDKIT_STEP_LOG_MAX_SIZE=-1
  fi

  docker build  --network host ${DockerBuildArgs} -t ${ECR_STR}/${DockerImageRepoGroup}/${PROJECT_NAME}:${VERSION} .

  if [[ $? == '0' ]];then
    echo ${PROJECT_NAME} image has builded.
    echo '##################################################'
  else
    echo error1
    exit 1
  fi
}

DockerImagePush(){

  docker push ${ECR_STR}/${DockerImageRepoGroup}/${PROJECT_NAME}:${VERSION}

}


DeployToEks(){
if [[  -f ~/.kube/config ]] &&  [[ $(which kubectl) ]];then
  kubectl apply -f AppDeployment.yml 
else
  echo "Please fix kubectl or conf"
fi
}

CleanWorkSpaceForBuild(){
  echo 'Cleaned WorkSpace For Build'
  mkdir -p /tmp/bsd_build_tmp/${PROJECT_NAME}
  echo '' > /tmp/mavenbuild.log
  if [[ ${BuildMethod} == 'docker' ]];then
    CleanFilesWhenBuildWithDocker=( 'xx.xx' )
  fi

  if [[ ${BuildMethod} == 'local' ]];then
    CleanFilesWhenBuildAtLocal=( './dist' )
  fi

  mv  Dockerfile AppDeployment.yml .dockerignore entrypoint.sh mavenbuildscript.sh ${CleanFilesWhenBuildAtLocal}  ${CleanFilesWhenBuildWithDocker}  /tmp/bsd_build_tmp/${PROJECT_NAME}/
}


for i in ${All_Project_Will_Deploy};do
  PROJECT_NAME=$i
  echo $PROJECT_NAME
  cd $PROJECT_NAME
  if [[ ${BuildMethod} == 'docker' ]];then
    echo 'will build  NodeJs with docker'
  elif [[ ${BuildMethod} == 'local' ]];then
    echo 'will build  NodeJs at local'
    NpmBuildNodeJs
  fi
	ls -lha && pwd
	GenerateNodejsEntrypoint
  GenerateNodejsDockerfile
  cat Dockerfile
  echo '#################################################'
  DockerBuild
  DockerImageRepoAuthSetForAwsEcr
  DockerImagePush
  GenerateAppDeployment
  DeployToEks
  CleanWorkSpaceForBuild
  #准备下一次循环
  cd ${BasicWorkDir}
done