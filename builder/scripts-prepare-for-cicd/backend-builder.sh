#!/bin/bash
# ./backend-builder.sh "iot-product-center iot-device-center iot-ota-center iot-data-center"

BasicWorkDir=$(pwd)
All_Project_Will_Deploy=$1
DockerImageRepoGroup=iot-dev
DockerImageName=''
ECR_STR="680218613974.dkr.ecr.us-east-1.amazonaws.com"
AWS_REGION="us-east-1"
EKS_CLUSTER_PREFIX="PositecEksDev"
#PROJECT_NAME="hello-go-demo"
CONTAINER_NAME="${DockerImageRepoGroup}/${PROJECT_NAME}"
#CONTAINER_IMAGE="$CONTAINER_NAME:$VERSION"
CreateTime=$(date +%Y%m%d-%H%M)
VERSION=dev-${CreateTime}
#在编译时使用国内源
IsInChina=true
#编译源码方式有docker,local 两种
#export BuildMethod=docker
export BuildMethod=local
#部署环境
export IotAppEnv=dev

##########################
#Maven build所需变量
#V1 通过命令查找所需要构建的项目pom文件;V2 通过mvn命令管理子模块方式构建
MavenBuildVersion=V2
#AllPomFileFullPath=$(find ${BasicWorkDir} -name *.jar)
AllPomFileFullPath=$(find ${BasicWorkDir} -name pom.xml)
#按照字符长度获取最上层pom所在完整绝对路径
TopPomDirFullPath=$(find ${BasicWorkDir} -name pom.xml|awk '{print length(), $0 | "sort -n" }'|head -n1|awk '{print $2}'|xargs dirname)

#获取当前路径下所有pom相对路径,主要docker build拷贝文件时候需要
AllPomFileRelativePath=$(find ./ -name pom.xml)
#按照字符长度获取最上层pom所在完整相对路径,主要docker build拷贝文件时候需要
TopPomDirRelativePath=$(find ./ -name pom.xml|awk '{print length(), $0 | "sort -n" }'|head -n1|awk '{print $2}'|xargs dirname)
#########################

if [[ ! $1 ]];then
	echo 'No PROJECT WILL Deploy,exiting....'
	exit 1
fi

#获取最新代码
git pull 


#每次执行脚本都进行common和config编译
cd /root/bsd-code/cloud/positec-global-iot-platform/positec-commons
mvn clean install  -Dmaven.skip.test=true
cd /root/bsd-code/cloud/positec-global-iot-platform/positec-config
mvn clean install  -Dmaven.skip.test=true



GenerateAppDeploymentConfigmap(){
if [[ ${IotAppEnv} == 'dev' ]];then
echo '正在生成dev环境的configmap'
cat > AppDeploymentConfigmap.yaml <<EOF
apiVersion: v1
kind: ConfigMap
metadata:
  name: iot-app-sys-env
data:
  IOT_NACOS_ADDR: '10.3.50.106:8848'
  IOT_NACOS_NAMESPACE: 'aea7e3f3-5a7d-4b8a-9a3f-487e8ad51c9e'
  IOT_MYSQL_URL: 'jdbc:mysql://10.3.50.76/db_iot_dev?useUnicode=true&characterEncoding=utf8&useSSL=false'
  IOT_MYSQL_USERNAME: 'thundersoftAdmin'
  IOT_MYSQL_PASSWORD: 'hPg47ucSW/O7O3bVaH+Tf63tOURdW8BF7MagekNCMriVrWCttHjCMO++9U4/Foi3RH9U9CVOwSsPn4xIb3S9CA=='
  IOT_MYSQL_PUBLICKEY: 'MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKWiLBMcP+CsB1WaalR8mPQZhSPJOhp3a/yel8YLZMS+uOW0ljLLqRLPlQrnsAGf8IkdYCEt/nwU6FzedySqQ4sCAwEAAQ=='
  IOT_REDIS_HOST: '10.3.50.56'
  IOT_REDIS_PORT: '6379'
  IOT_REDIS_PASSWORD: ''
  IOT_MONGO_URI: 'mongodb://docdbAdmin:DocDBAdmin#123@10.3.50.73:27017/positecDb?connectTimeoutMS=5000000&retryWrites=false'
  IOT_ES_HOST: 'vpc-positeceksdev-cluster-es-ufpoxqw3dfhcunl26nkg7vkrc4.us-east-1.es.amazonaws.com'
  IOT_ES_PORT: '443'
  IOT_ES_SCHEME: 'https'
  IOT_AWS_ACCESSID: 'AKIAZ4YBQLDLIBS5HEOC'
  IOT_AWS_SECRETKEY: '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'
  IOT_AWS_REGION: 'us-east-1'
  IOT_AWS_RULE_TOKEN_NAME: 'rule_token'
  IOT_AWS_RULE_TOKEN: '06f7cdbc-20b5-404f-a2ba-309771602d05'
  IOT_AWS_POLICY_NAME: 'global_device_policy'
  IOT_AWS_PINPOINT_APPID: 'c50e76c2e42b4a72a832c7ce249819a0'
  IOT_MQTT_URL: 'ssl://b-4d47f2c4-7632-4abb-be92-6543c7846908-1.mq.us-east-1.amazonaws.com:8883'
  IOT_MQTT_USERNAME: 'activemqAdmin'
  IOT_MQTT_PASSWORD: 'activemqAdmin@123'
  IOT_CUSTOM_ACTIVEURL: 'http://obms-commercial.positecgroup.com/invite/'
  IOT_CUSTOM_TEMPORARYURL: '/usr/local/temporary/'
  IOT_CUSTOM_AESPASSWORD: 'positec_password'
  IOT_CUSTOM_MAPDISTANCE: '200'
  IOT_CUSTOM_TOKENNAME: 'access_token'
  IOT_CUSTOM_GEO_API_KEY: 'AIzaSyCnJvQJ4Z4_oxZYs8qclG2EwPsjElIKuww'
  IOT_MAIL_HOST: 'smtphz.qiye.163.com'
  IOT_MAIL_USERNAME: 'wanghao1224@thundersoft.com'
  IOT_MAIL_PASSWORD: 'wang.1281615079'
  IOT_MAIL_PORT: '465'
  IOT_LOG_LEVEL_POSITEC: 'debug'
  IOT_ASPECT_LOG_SWITCH: 'true'
EOF
fi
}


MavenSettings(){
echo "Generating MavenSettings"
cat > settings.xml << EOF
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
  <servers>
    <server>
      <id>releases</id>     <!-- 要与pom.xml的发布仓库中的id对应 -->
      <username>admin</username>
      <password>123456</password>
    </server>
    <server>
        <id>snapshots</id>  <!-- 要与pom.xml的发布仓库中的id对应 -->
        <username>admin</username>
        <password>admin123</password>
    </server>
  </servers>

  <mirrors>
    <mirror>
        <id>nexus-aliyun1</id>
        <mirrorOf>central</mirrorOf>
        <name>Nexus aliyun1</name>
        <url>http://maven.aliyun.com/nexus/content/groups/public</url>
    </mirror>

    <mirror>
        <id>nexus-aliyun2</id>
        <mirrorOf>apache-maven</mirrorOf>
        <name>Nexus aliyun2
        </name>
        <url>http://maven.aliyun.com/nexus/content/groups/public</url>
    </mirror>
  </mirrors>

</settings>
EOF

if [[ $BuildMethod == 'local' ]];then
  if [[ ! -d /usr/share/maven/ref1/ ]];then
    mkdir -p /usr/share/maven/ref1/
  fi
  mv ./settings.xml /usr/share/maven/ref1/
fi
}



MavenBuildV1(){
echo "MavenV1 building at local"
if [[ $IsInChina == 'true' ]];then
  MavenSettings
  MavenBuildArgs='-s /usr/share/maven/ref1/settings.xml'
fi
  #-U: Forces a check for missing releases and updated snapshots on remote repositories,mvn deploy的时候建议带上
  mvn clean install ${MavenBuildArgs} -Dmaven.skip.test=true 
}


MavenBuildV2(){
echo "MavenV2 building at local"
if [[ $IsInChina == 'true' ]];then
  MavenSettings
  MavenBuildArgs='-s /usr/share/maven/ref1/settings.xml'
fi

#-U: Forces a check for missing releases and updated snapshots on remote repositories,mvn deploy的时候建议带上
echo '下面将在当前路径开始Maven编译'
echo '' > /tmp/mavenbuild.log
mvn clean install  -Dmaven.skip.test=true  ${MavenBuildArgs} --projects ${PROJECT_NAME} -am 2>&1 |tee  /tmp/mavenbuild.log
tail  /tmp/mavenbuild.log|grep -Ei  'ERROR'
if [[ $? -eq 0 ]];then
  echo "子模块${PROJECT_NAME}构建失败,下面将尝试项目根目录进行全量编译"
  cd ${TopPomDirFullPath}
  echo '' > /tmp/mavenbuild.log
  mvn clean install  ${MavenBuildArgs} -Dmaven.skip.test=true |tee  /tmp/mavenbuild.log
  tail  /tmp/mavenbuild.log|grep -Ei  'ERROR'
  if [[ $? -eq 0 ]];then
    echo "所有模块${PROJECT_NAME}构建成功"
  fi
else
  echo "子模块${PROJECT_NAME}构建成功"
fi
echo "当前所在目录为${TopPomDirFullPath}"
}

#容器里mvn build使用
GenerateMavenBuildScripts(){
#下面脚本将在maven编译单个子模块失效时，依据顶层Pom.xml进行所有jar文件编译打包
cat > mavenbuildscript.sh << EOF
#!/bin/bash
set -x
ls -la 

BasicWorkDir=\$(pwd)

#cd ${TopPomDirRelativePath}
#进入子模块pom.xml文件所在路径
cd ${PomDirRelativePath}
#进入到子模块的父级目录中
cd ../


echo "MavenV2 building at docker"
if [[ $IsInChina == 'true' ]];then
  #MavenSettings
  MavenBuildArgs='-s /usr/share/maven/ref1/settings.xml -B '
  echo ''
fi

#-U: Forces a check for missing releases and updated snapshots on remote repositories,mvn deploy的时候建议带上
echo '下面将在当前路径开始Maven编译'
echo '' > /tmp/mavenbuild.log
mvn clean install  -Dmaven.skip.test=true  \${MavenBuildArgs} --projects ${PROJECT_NAME} -am  2>&1 |tee  /tmp/mavenbuild.log
tail  /tmp/mavenbuild.log|grep -E  '\[ERROR\]'
if [[ ! \$? -eq 0 ]];then
  echo "子模块${PROJECT_NAME}构建成功"
else
  echo "子模块${PROJECT_NAME}构建失败,下面将尝试项目根目录进行全量编译"
  cd \${BasicWorkDir}
  cd ${TopPomDirRelativePath}
  echo '' > /tmp/mavenbuild.log
  mvn clean install  \${MavenBuildArgs} -Dmaven.skip.test=true 2>&1 |tee  /tmp/mavenbuild.log
  tail  /tmp/mavenbuild.log|grep -E  '\[ERROR\]' 
  if [[ ! \$? -eq 0 ]];then
    echo "所有模块${PROJECT_NAME}构建成功"
  fi
fi
echo "当前所在目录为\$(pwd)"
cd \${BasicWorkDir}
cd ${PomDirRelativePath}/target/
JarFileName=\$(ls |grep -E "${PROJECT_NAME}.*.jar$" ) 
echo "JarFileName      ---------------->   \$JarFileName" 
ls -la && pwd
cp \${JarFileName} /tmp/
EOF
}


GenerateJavaDockerfile(){
echo "Generating JavaDockerfile"

if [[ $BuildMethod == 'local' ]];then
#DockerfileV1使用编译好的文件进行镜像制作
cat > .dockerignore <<EOF
node_modules
.m2/repository
EOF

cat > Dockerfile << EOF
FROM frolvlad/alpine-java:jdk8.202.08-cleaned
#FROM sgrio/java:jdk_8_alpine
ENV AppFileName $JarFileName
#ENV TdsEnv $TdsEnv
#ENV JavaOptions $JavaOption
ENV TZ Etc/UTC
RUN ln -fs /usr/share/zoneinfo/\${TZ} /etc/localtime \
    && echo \${TZ} > /etc/timezone
RUN mkdir -p /project/logs
COPY \${AppFileName}  /project/
#COPY *.properties /project/config/
COPY entrypoint.sh /project/
RUN chmod +x /project/entrypoint.sh
#COPY ./DnsList.txt /project/
WORKDIR /project
CMD ["sh","-x","/project/entrypoint.sh"]
EOF
fi

if [[ $BuildMethod == 'docker' ]];then
cat > .dockerignore <<EOF
.m2/repository
EOF


GenerateMavenBuildScripts

cat > Dockerfile << EOF
FROM maven:3-openjdk-11 AS builder
WORKDIR /project
#RUN --mount=type=cache,id=build_for_mvn_module,target=~/.m2
COPY ${TopPomDirRelativePath}  ${TopPomDirRelativePath}
COPY ./README.md ./
#为了控制是否使用settings.xml,因此将默认配置文件路径/usr/share/maven/ref放到了其他位置
COPY ./settings.xml /usr/share/maven/ref1/
#COPY ./backend-builder.sh  ./
COPY ./mavenbuildscript.sh ./
RUN --mount=type=cache,id=build_for_mvn_module,target=/root/.m2 chmod +x ./mavenbuildscript.sh && ./mavenbuildscript.sh
#RUN  chmod +x ./mavenbuildscript.sh && ./mavenbuildscript.sh
#RUN  source ./backend-builder.sh && MavenBuildV2


FROM frolvlad/alpine-java:jdk8.202.08-cleaned
#FROM sgrio/java:jdk_8_alpine
#ENV TdsEnv $TdsEnv
#ENV JavaOptions $JavaOption
ENV AppFileName $JarFileName
ENV TZ Etc/UTC
RUN ln -fs /usr/share/zoneinfo/\${TZ} /etc/localtime \
    && echo \${TZ} > /etc/timezone
RUN mkdir -p /project/logs
#COPY \${AppFileName}  /project/
COPY --from=builder /tmp/.  /project/
#COPY *.properties /project/config/
COPY entrypoint.sh /project/
RUN chmod +x /project/entrypoint.sh
#COPY ./DnsList.txt /project/
WORKDIR /project
CMD ["sh","-x","/project/entrypoint.sh"]
EOF
fi
}

GenerateJavaEntrypoint(){
echo "Generating JavaEntrypoint"
# 变量前的'\'在EOF中,不要删除
cat > entrypoint.sh << EOF
#!/bin/sh

#cat /project/DnsList.txt >> /etc/hosts


java  \$JavaOptions  -jar \$AppFileName

EOF
}


GenerateAppDeployment(){
echo "Generating AppDeployment.yml"

#获取服务端口
ContainerPort=$(cat ${BasicWorkDir}/README.md |grep ${PROJECT_NAME}|grep -Eo "[0-9]+")

# 变量前的'\'在EOF中,不要删除
cat > AppDeployment.yml << EOF
apiVersion: v1
kind: Namespace
metadata:
  name: default
---
apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: default
  name: ${PROJECT_NAME}
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: ${PROJECT_NAME}
  replicas: 1
  template:
    metadata:
      labels:
        app.kubernetes.io/name: ${PROJECT_NAME}
    spec:
      containers:
      - image: ${ECR_STR}/${DockerImageRepoGroup}/${PROJECT_NAME}:${VERSION}
        resources:
          limits:
            cpu: "1"
            memory: 2048Mi
          requests:
            cpu: "1"
            memory: 2048Mi
        envFrom:
        - configMapRef:
            name: iot-app-sys-env
        imagePullPolicy: Always
        name: ${PROJECT_NAME}
        ports:
        - containerPort: $ContainerPort
---
apiVersion: v1
kind: Service
metadata:
  namespace: default
  name: ${PROJECT_NAME}
spec:
  ports:
    - port: ${ContainerPort}
      targetPort: ${ContainerPort}
      protocol: TCP
  type: ClusterIP
  selector:
    app.kubernetes.io/name: ${PROJECT_NAME}
EOF
cat AppDeployment.yml
}

DockerImageRepoAuthSetForAwsEcr(){
#  if [[ $(cat ~/.docker/config.json |grep ${ECR_STR})  ]];then
#    echo "you can already push images to $ECR_STR"
#  else
    #令牌有效时间为12小时,因此建议每次运行脚本时候生成一次
    echo "set docker image repo auth for  $ECR_STR/${DockerImageRepoGroup}/${PROJECT_NAME}"
    aws ecr get-login-password --region ${AWS_REGION} | docker login --username AWS --password-stdin $ECR_STR
#  fi
  aws --region ${AWS_REGION} ecr describe-repositories --repository-names ${DockerImageRepoGroup}/${PROJECT_NAME} || aws --region ${AWS_REGION} ecr create-repository --repository-name ${DockerImageRepoGroup}/${PROJECT_NAME}
}


DockerBuild(){
  echo "Building docker image"
  if [[ $BuildMethod == 'docker' ]];then
    #docker版本大于19.03，才能启用buildkit
    DockerVersion=$(docker -v|awk '{print $3}'|tr -d ',')
    export DOCKER_BUILDKIT=1
    #出现output clipped, log limit 1MiB reached问题
    DockerBuildArgs='--progress=plain'
    #DockerBuildMethod='buildx'
  fi
  docker ${DockerBuildMethod} build --network host $DockerBuildArgs  -t ${ECR_STR}/${DockerImageRepoGroup}/${PROJECT_NAME}:${VERSION} .
  if [[ $? == '0' ]];then
    echo ${PROJECT_NAME} image has builded.
    echo '##################################################'
  else
    echo error1
    exit 1
  fi
}

DockerImagePush(){
  echo "Pushing docker image"
  docker push ${ECR_STR}/${DockerImageRepoGroup}/${PROJECT_NAME}:${VERSION}

}


DeployToEks(){
if [[  -f ~/.kube/config ]] &&  [[ $(which kubectl) ]];then
  GenerateAppDeploymentConfigmap
  kubectl apply -f AppDeploymentConfigmap.yaml
  kubectl apply -f AppDeployment.yml 
else
  echo "Please fix kubectl or conf"
fi
}

CleanWorkSpaceForBuild(){
  echo 'Cleaned WorkSpace For Build'
  mkdir -p /tmp/bsd_build_tmp/${PROJECT_NAME}
  echo '' > /tmp/mavenbuild.log
  if [[ ${BuildMethod} == 'docker' ]];then
    CleanFilesWhenBuildWithDocker=( 'settings.xml' 'mavenbuildscript.sh')
  fi

  if [[ ${BuildMethod} == 'local' ]];then
    CleanFilesWhenBuildAtLocal=( '/usr/share/maven/ref1/settings.xml' )
  fi

  mv  Dockerfile AppDeployment.yml .dockerignore entrypoint.sh mavenbuildscript.sh ${CleanFilesWhenBuildAtLocal}  ${CleanFilesWhenBuildWithDocker}  /tmp/bsd_build_tmp/${PROJECT_NAME}/
}


for PROJECT_NAME in ${All_Project_Will_Deploy};do
	echo "PROJECT_NAME     ---------------->   $PROJECT_NAME"
  #当前子项目pom文件完整路径
	export PomFileFullPath=$(echo ${AllPomFileFullPath}|tr ' ' '\n'|grep $PROJECT_NAME)
	echo "PomFileFullPath  ---------------->   $PomFileFullPath"
  #当前子项目pom文件所在目录完整路径
  export PomDirFullPath=$(dirname ${PomFileFullPath}|head -n1)
	echo "PomDirFullPath  ---------------->   $PomDirFullPath"


  #当前子项目pom文件相对路径，docker里构建时需要
	export PomFileRelativePath=$(echo ${AllPomFileRelativePath}|tr ' ' '\n'|grep $PROJECT_NAME)
	echo "PomFileRelativePath  ---------------->   $PomFileRelativePath"
  #当前子项目pom文件所在目录相对路径，docker里构建时需要
  export PomDirRelativePath=$(dirname ${PomFileRelativePath}|head -n1)
	echo "PomDirRelativePath  ---------------->   $PomDirRelativePath"

  MavenSettings
  if [[ ${BuildMethod} == 'docker' ]];then
    echo 'will build  with docker'
    if [[ ${MavenBuildVersion} == 'V1' ]];then
      #MavenBuildV1
      #cd ./target
      echo "暂不支持，${MavenBuildVersion} 方式"
      exit 1
    elif [[ ${MavenBuildVersion} == 'V2' ]];then
      echo "下面将通过docker 编译${PROJECT_NAME}项目/子模块源码"
      # 需要注入如下变量，供生成Dockerfile时使用
      export JarFileName="${PROJECT_NAME}-1.0-SNAPSHOT.jar"
    fi
  fi

  if [[ ${BuildMethod} == 'local' ]];then
    cd ${PomDirFullPath}
    echo 'will build at local'
    if [[ ${MavenBuildVersion} == 'V1' ]];then
      MavenBuildV1
    elif [[ ${MavenBuildVersion} == 'V2' ]];then
      #进到上层父级POM.xml所在目录
      cd ../
      MavenBuildV2
      # GenerateMavenBuildScripts
      # bash +x ./mavenbuildscript.sh 
    fi
    echo "下面将切换到子模块jar包所在路径${PomDirFullPath}"
    cd ${PomDirFullPath}/target
    export JarFileName=$(ls |grep -E "${PROJECT_NAME}.*.jar$" )
	  echo "JarFileName      ---------------->   $JarFileName"
  fi

  ls -la && pwd
  
  GenerateJavaEntrypoint
  GenerateJavaDockerfile
  cat Dockerfile
  echo '#################################################'
  ls -lha && pwd
  DockerBuild
  DockerImageRepoAuthSetForAwsEcr
  DockerImagePush
  GenerateAppDeployment
  DeployToEks
  CleanWorkSpaceForBuild
  #准备下一次循环
  cd ${BasicWorkDir}
done
