# EKS/docker for application cicd

# 镜像说明
构建基础镜像-DID
```
关于 dockerfile-docker-dind.yml  文件，可用参考：https://github.com/pingod/dockerfiles_and_yaml/tree/master/alpine-arm


docker build --network host -f dockerfile-docker-dind.yml -t registry.cn-hangzhou.aliyuncs.com/sourcegarden/aws-docker:20-dind .
```

构建时用的基础镜像为：registry.cn-hangzhou.aliyuncs.com/sourcegarden/aws-docker:20-dind,该镜像包含了aws-cli 、kubectl(1.21) 、以及awscli 操作时所需要的环境变量（目前value并未具体设置），同时，我也将上面镜像上传到下面几个镜像仓库中：
registry.gitlab.com/gitlab-pipeline-share/pipelines-for-gitlab:aws-docker-20-dind
680218613974.dkr.ecr.us-east-1.amazonaws.com/iot-dev/aws-docker:20-dind


# 流水线说明
1. 目录流水中同时构建多个子模块通过下面trigger方式解决，如下所示:
```
init_gateway:
  stage: init_pipeline
  variables:
    PROJECT_NAME: gateway
  # script:
  #   - echo "PROJECT_NAME=gateway" >> build.env
  trigger:
    include:
      - project: 'gitlab-pipeline-share/pipelines-for-gitlab'
        ref: master
        file: 'builder/eks-template/backend-java-k8s.yml'
  # artifacts:
  #   reports:
  #     dotenv: build.env
  only:
    changes:
      - "hello-java-demo02/gateway/**/*"  
```

2. 同时构建多模块还有另外一个思路：通过流水线中脚本curl 对应流水线API,并在curl过程中post所需的变量/参数给流水线API(当然,这种方式属于一种较旧的方式,若果要使用,请参考：https://docs.gitlab.com/ee/ci/triggers/)


# 使用约束条件
1. 使用前需要将AWS的AK/SK 或 一些加密环境变量填写到gitlab的全局变量设置中(一般的建议设置到项目的群组中)
2. 代码仓库的根目录可以不为项目根目录
3. 


# 其他
## 参考材料

https://github.com/SocialGouv/gitlab-ci-yml

*推荐*
https://github.com/Ouest-France/cdp

*推荐: Multi-project pipelines*
https://www.youtube.com/watch?v=g_PIwBM1J84

视频对应的代码仓库(作者是个深度gitlab使用者,建议多扒扒他的仓库)：

![图 1](images/README/IMG_20220406-164932238.png)  


示例一：https://gitlab.com/fpotter/examples/multi-project-example-1-code-repo/-/blob/master/.gitlab-ci.yml

示例二: https://gitlab.com/fpotter/examples/multi-project-orchestrator

示例三: https://gitlab.com/fpotter/examples/multi-project-specific-version-parent

*推荐: 官方Multi-project pipelines说明*
https://docs.gitlab.com/ee/ci/pipelines/multi_project_pipelines.html#creating-multi-project-pipelines-from-gitlab-ciyml

https://docs.gitlab.cn/jh/ci/pipelines/multi_project_pipelines.html


## 一些废话
1. java编译如果在有私服的情况下，其实脚本可以写的很简单，不用去考虑子模块编译时候所需依赖是否已经编译成功
2. 如果一个代码仓库，只存放一个maven 子模块，那脚本也不用写这么复杂
3. 如果只考虑编译整个项目，不考虑编译单个子模块，那脚本也可以写的不这么复杂
4. 现在还无法做到所有变量都提取到一个文件中